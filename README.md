# SMC Fonts packages 

This repository contains the AUR PKGBUILDS for the fonts that SMC has released so far. 

In addition to individual font packages, it also contains a meta package.

# Installing using yaourt

To install all fonts in one go, issue this command. 

yaourt -S ttf-malayalam-fonts-meta

Additional switches are available to make yaourt non interactive, explore if you like to.

You can also install the individual packages. Folllowing commands will install all fonts as well. 

yaourt -S ttf-malayalam-font-anjalioldlipi
yaourt -S ttf-malayalam-font-chilanka
yaourt -S ttf-malayalam-font-dyuthi
yaourt -S ttf-malayalam-font-keraleeyam
yaourt -S ttf-malayalam-font-meera
yaourt -S ttf-malayalam-font-rachana
yaourt -S ttf-malayalam-font-raghumalayalamsans
yaourt -S ttf-malayalam-font-suruma

